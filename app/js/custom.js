$(window).on('load', function () {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
        $('body').addClass('ios');
    } else {
        $('body').addClass('web');
    };
    $('body').removeClass('loaded');
    var isshow = localStorage.getItem('isshow');
    if (isshow == null) {
        localStorage.setItem('isshow', 1);
        // Show popup here
        $('.js-fancybox').click();
    }
});

/* viewport width */
function viewport() {
    var e = window,
        a = 'inner';
    if (!('innerWidth' in window)) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return {
        width: e[a + 'Width'],
        height: e[a + 'Height']
    }
};
/* viewport width */
$(function () {
    /* placeholder*/
    $('input, textarea').each(function () {
        var placeholder = $(this).attr('placeholder');
        $(this).focus(function () {
            $(this).attr('placeholder', '');
            return false;
        });
        $(this).focusout(function () {
            $(this).attr('placeholder', placeholder);
            return false;
        });
        var val_field = $(this).val().length;

        if (val_field >= 1) {
            $(this).parents('.box-field').addClass('focused-field');
        } else {
            $(this).parents('.box-field').removeClass('focused-field');
        };
    });
    $('.form-control').focus(function () {
        $(this).parents('.box-field').addClass('focused-field');
    });
    $('.form-control').focus(function () {
        $(this).parents('.box-field').addClass('focused-field');
    });
    $('.form-control').focusout(function () {
        var val_field = $(this).val().length;

        if (val_field >= 1) {
            $(this).parents('.box-field').addClass('focused-field');
        } else {
            $(this).parents('.box-field').removeClass('focused-field');
        };
    });
    /* placeholder*/
    //js-box-seotext
    $(".js-box-seotext").click(function () {
        $(this).toggleClass('active');
        $('.box-seotext_short').toggle();
        $('.box-seotext_full').toggle();
        return false;  
    });

    // 
    $('.js-check-subsc').each(function() {
        if($(this).find('input').is(":checked")) {
            $(this).find('.label-text').text("Отписаться");
        }
    });
    $('.js-check-subsc input').change(function() {
        if($(this).is(":checked")) {
            $(this).parent('.js-check-subsc').find('.label-text').text("Отписаться");
        }
        else {
            $(this).parent('.js-check-subsc').find('.label-text').text("Подписаться");
        }
    });

    
    /*show result search*/
    $('.js-show-result').each(function () {
        $(this).focus(function () {
            $(this).parents('form').addClass('show-result');
            return false;
        });
        $(this).focusout(function () {
            $(this).parents('form').removeClass('show-result');
            return false;
        });
    });
    /*show result search*/
    /*hover sub nav begin*/
    $('.js-main-nav-desc').hover(function () {
        $('.main-nav-list__item').removeClass('active');
        $(this).addClass('active');
        var dropId = $(this).find('a').attr('href');
        $('.drop-nav').removeClass('show-drop');
        $(dropId).addClass('show-drop');
    }, function () {

    });
    var timer;
    $('.js-main-nav').hover(function () {
        var dropNav = $(this);
        timer = setTimeout(function () {
            $(dropNav).addClass('active-nav');
        }, 200);
    }, function () {
        $('.js-main-nav').removeClass('active-nav');
        $('.main-nav-list__item').removeClass('active');
        clearTimeout(timer);
    });
    /*hover sub nav end*/
    /*extra nav begin*/
    $('.js-extra-nav').click(function () {
        $(this).toggleClass('active');
        $('.box-extra-nav').toggleClass('opened');
        return false;
    });
    $(document).on('touchstart click', function (e) {
        if ($(e.target).parents().filter('.box-extra-nav:visible').length != 1) {
            $('.js-extra-nav').removeClass('active');
            $('.box-extra-nav').removeClass('opened');
        }
    });
    /*extra nav end*/
    /*auth begin*/
    $('.js-auth').click(function () {
        $('.box-sign-in').toggleClass('opened');
        return false;
    });
    $(document).on('touchstart click', function (e) {
        if ($(e.target).parents().filter('.box-sign-in:visible').length != 1) {
            $('.box-sign-in').removeClass('opened');
        }
    });
    /*auth end*/
    /*extra nav begin*/
    $('.js-mob-nav').click(function () {
        $(this).toggleClass('active'),
            $('.box-mob-nav').toggleClass('opened');
        $('header').toggleClass('opened-nav');
        $('body').toggleClass('no-scroll');
        $('html').toggleClass('no-scroll');
        return false;
    });
    /*extra nav end*/
    /*sub nav begin*/
    $('.js-sub-nav-link').click(function () {
        $('header').addClass('opened-sub-mob');
        $(this).closest('.main-nav-list__item').children().addClass('show-sub-mob');
        return false;
    });
    $('.js-sub-nav-close').click(function () {
        $('header').removeClass('opened-sub-mob');
        $(this).closest('.main-nav-list_sub-mob').removeClass('show-sub-mob');
        return false;
    });
    $('.js-sub-sub-nav-close').click(function () {
        $(this).closest('.main-nav-list_sub-mob').removeClass('show-sub-mob');
        return false;
    });
    /*sub nav end*/
    /*search mob begin*/
    $('.js-search-mob').click(function () {
        $('.box-search').addClass('opened');
        $('.js-mob-nav').removeClass('active'),
            $('.box-mob-nav').removeClass('opened');
        $('header').removeClass('opened-nav');
        $('body').removeClass('no-scroll');
        $('html').removeClass('no-scroll');
        return false;
    });
    $('.js-search-hide').click(function () {
        $('.box-search').removeClass('opened');
        return false;
    });
    $(document).on('touchstart click', function (e) {
        if ($(e.target).parents().filter('.box-search:visible').length != 1) {
            $('.box-search').removeClass('opened');
        }
    });
    /*search mob end*/
    /*img om background begin*/
    $('.js-image img').each(function (index, element) {
        var image = $(this).attr('src');
        $(this).parent('.js-image').css('background-image', 'url(' + image + ')');
        $(this).remove();
    });
    /*img om background end*/
    /*cutom select begin*/
    $('.js-select-title').click(function () {
        $(this).parents('li').siblings().find('.cust-select').removeClass('opened');
        $(this).parents('.cust-select').toggleClass('opened');
        return false;
    });
    $(document).on('touchstart click', function (e) {
        if ($(e.target).parents().filter('.cust-select:visible').length != 1) {
            $('.cust-select').removeClass('opened');
        }
    });
    $('.js-select-list li a').click(function () {
        $(this).parent().siblings().removeClass('active');
        var id = $(this).attr('href');
        var idText = $(this).text();
        $(this).parent().addClass('active');
        $(this).parents('.cust-select').removeClass('opened');
        $(this).parents('.cust-select').find('.js-select-title span').text(idText);
        return false;
    });
    $('.js-select-title-hover').hover(function () {
        $(this).addClass('opened');
    }, function () {
        $(this).removeClass('opened');
    });
    /*cutom select begin*/
    /*hidden text on catalog begin*/
    $(".js-more").toggle(function () {
        $(this).parents('p').find('.hide-text-inline').addClass('show-text');
        $(this).addClass('active');
    }, function () {
        $(this).parents('p').find('.hide-text-inline').removeClass('show-text');
        $(this).removeClass('active');
    });
    /*hidden text on catalog end*/
    /*more item list begin*/

    $('.subcategories-hidden').each(function () {
        var counterHeight = 0;
        var counter = 1;
        $(this).find('.subcategories__item').each(function () {
            if (counter === 6) {
                return false;
            } else {
                counterHeight += $(this).outerHeight();
                counter++;
            }
        });
        $(this).height(counterHeight);
    });


    /*$(".js-all-item").toggle(function () {
        var listHeight = $(this).parents('.filter-item__cont').find('.js-subcategories').outerHeight();
        $(this).parents('.filter-item__cont').find('.subcategories-hidden').css('height', listHeight);
        $(this).find('span').text('Меньше категорий');
        $(this).addClass('active');
    }, function () {
        var counterHeight = 0;
        var counter = 1;
        $(this).parents('.filter-item__cont').find('.subcategories__item').each(function () {
            if (counter === 6) {
                return false;
            } else {
                counterHeight += $(this).outerHeight();
                counter++;
            }
        });
        $(this).parents('.filter-item__cont').find('.subcategories-hidden').css('height', counterHeight);
        $(this).find('span').text('Все категории');
        $(this).removeClass('active');
    });*/
    function itemhandler1() {
        var listHeight = $(this).parents('.filter-item__cont').find('.js-subcategories').outerHeight();
        $(this).parents('.filter-item__cont').find('.subcategories-hidden').css('height', listHeight);
        $(this).find('span').text('Меньше категорий');
        $(this).addClass('active');
        $(this).one("click", itemhandler2);
        return false;
    }
    function itemhandler2() {
        var counterHeight = 0;
        var counter = 1;
        $(this).parents('.filter-item__cont').find('.subcategories__item').each(function () {
            if (counter === 6) {
                return false;
            } else {
                counterHeight += $(this).outerHeight();
                counter++;
            }
        });
        $(this).parents('.filter-item__cont').find('.subcategories-hidden').css('height', counterHeight);
        $(this).find('span').text('Все категории');
        $(this).removeClass('active');
        $(this).one("click",itemhandler1);
        return false;
    }
    $(".js-all-item").one("click", itemhandler1);

   /* $(".js-all-brand").toggle(function () {
        var listHeight = $(this).parents('.filter-item__cont').find('.js-subcategories').outerHeight();
        $(this).parents('.filter-item__cont').find('.subcategories-hidden').css('height', listHeight);
        $(this).find('span').text('Свернуть');
        $(this).addClass('active');
    }, function () {
        var counterHeight = 0;
        var counter = 1;
        $(this).parents('.filter-item__cont').find('.subcategories__item').each(function () {
            if (counter === 6) {
                return false;
            } else {
                counterHeight += $(this).outerHeight();
                counter++;
            }
        });
        $(this).parents('.filter-item__cont').find('.subcategories-hidden').css('height', counterHeight);
        $(this).find('span').text('Все бренды');
        $(this).removeClass('active');
    });*/

    function brandhandler1() {
        var listHeight = $(this).parents('.filter-item__cont').find('.js-subcategories').outerHeight();
        $(this).parents('.filter-item__cont').find('.subcategories-hidden').css('height', listHeight);
        $(this).find('span').text('Свернуть');
        $(this).addClass('active');
        $(this).one("click", brandhandler2);
        return false;
    }
    function brandhandler2() {
        var counterHeight = 0;
        var counter = 1;
        $(this).parents('.filter-item__cont').find('.subcategories__item').each(function () {
            if (counter === 6) {
                return false;
            } else {
                counterHeight += $(this).outerHeight();
                counter++;
            }
        });
        $(this).parents('.filter-item__cont').find('.subcategories-hidden').css('height', counterHeight);
        $(this).find('span').text('Все бренды');
        $(this).removeClass('active');
        $(this).one("click", brandhandler1);
        return false;
    }
    $(".js-all-brand").one("click", brandhandler1);



    /*more item list end*/
    /*hidden text on goods begin*/
    /*$(".js-more-text").toggle(function () {
        $(this).parents('.box-hidden-text').find('.hide-text').slideDown();
        $(this).addClass('active').find('.more-link__text').text('Свернуть');
    }, function () {
        $(this).parents('.box-hidden-text').find('.hide-text').slideUp();
        $(this).removeClass('active').find('.more-link__text').text('читать полностью');
    });*/

    function morehandler1() {
        $(this).parents('.box-hidden-text').find('.hide-text').slideDown();
        $(this).addClass('active').find('.more-link__text').text('Свернуть');
        $(this).one("click", morehandler2);
        return false;
    }
    function morehandler2() {
        $(this).parents('.box-hidden-text').find('.hide-text').slideUp();
        $(this).removeClass('active').find('.more-link__text').text('читать полностью');
        $(this).one("click", morehandler1);
        return false;
    }
    $(".js-more-text").one("click", morehandler1);
    /*hidden text on goods end*/
    /*mob filter nav begin*/
    $('.js-filter-mob').click(function () {
        $('.catalog-aside').addClass('opened');
        $('body').addClass('no-scroll');
        $('html').addClass('no-scroll');
        return false;
    });
    $('.js-mob-filter-close').click(function () {
        $('.catalog-aside').removeClass('opened');
        $('body').removeClass('no-scroll');
        $('html').removeClass('no-scroll');
        return false;
    });
    $('.js-filter-item').click(function () {
        var id = $(this).attr('href');
        $(id).addClass('show-item');
        return false;
    });
    $('.js-filter-back').click(function () {
        $('.filter-item').removeClass('show-item');
        return false;
    });
    /*mob filter nav end*/
    /*favor begin*/

    $('.js-favor').click(function () {
        $(this).toggleClass('active');
        return false;
    });
    /*favor end*/
    /*all shop mob begin*/
    $('.js-all-shop').click(function () {
        $(this).toggleClass('active')
        $('.all-shop-list').slideToggle();
        return false;
    });
    /*all shop mob end*/
    /* tabs begin*/
    $('.js-tabs li a').click(function () {
        $(this).parents('.tab-wrap').find('.tab-cont').addClass('hide-tab');
        $(this).parent().siblings().removeClass('active');
        var id = $(this).attr('href');
        $(id).removeClass('hide-tab');
        $(this).parent().addClass('active');
        return false;
    });
    /* tabs end*/
    /*registration page begin*/
    $('.js-restore-link').click(function () {
        $('.tab-wrap').addClass('hide-tab');
        $('.restore-password-form').removeClass('hide-tab');
        return false;
    });
    $('.js-restore-link-hide').click(function () {
        $('.tab-wrap').removeClass('hide-tab');
        $('.restore-password-form').addClass('hide-tab');
        return false;
    });
    /*registration page end*/


    
    /*goods img begin*/
    $('.js-slider-sm').click(function () {
        $(this).parent().siblings().removeClass('active');
        $(this).parent().addClass('active');
        var imgSrc = $(this).find('img').attr('src');
        $('.googs-slider').find('.js-slider-big img').attr('src', imgSrc);
        return false;
    });
    $('.js-slider-sm-more').click(function () {
        $('.googs-img-more').addClass('show');
        if ($(document).height() > $(window).height()) {
            var scrollTop = ($('html').scrollTop()) ? $('html').scrollTop() : $('body').scrollTop();
            $('html').addClass('no-scroll-desc').css('top', -scrollTop);
        }
        return false;
    });
    $('.js-slider-sm-close').click(function () {
        $('.googs-img-more').removeClass('show');
        var scrollTop = parseInt($('html').css('top'));
        $('html').removeClass('no-scroll-desc');
        $('html,body').scrollTop(-scrollTop);
        return false;
    });
    /*goods img end*/
    /*scroll top begin*/
    $(".js-top").click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, "slow");
        return false;
    });
    /*scroll top end*/
    /*scroll id begin*/
    $('.js-scroll').click(function () {
        var target = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(target).offset().top - 30
        }, 1000);
        return false;
    });
    /*scroll id end*/
    /*title acordion goods begin*/
    $(".js-googs-mob-title").click(function () {
        $(this).toggleClass('active');
        $(this).next().slideToggle();
        return false;
    });
    /*title acordion goods end*/
    /*contents scroll id begin*/
    $('.js-contents a').click(function () {
        var target = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(target).offset().top
        }, 1000);
        return false;
    });
    /*contents scroll id end*/
    $('.filter-item__title').click(function () {
        $(this).toggleClass('active');
        $(this).next().slideToggle();
        return false;
    });

    if ($('.js-compare-slider').length) {
        setTimeout(function () {
            $('.js-compare-slider .compare-list__item .col').css('height', 'auto');
            var $countItem = $('.js-compare-slider .compare-list__item:last-child .col').length;
            for (var $i = 1; $i <= $countItem; $i++) {
                var $maxHeight = 0;
                $('.js-compare-slider .compare-list__item .col_' + $i).each(function () {
                    $maxHeight = $maxHeight > $(this).outerHeight() ? $maxHeight : $(this).outerHeight();
                });
                $('.compare .compare-list__item .col_' + $i).each(function () {
                    $(this).css('height', $maxHeight + 'px');
                });
            };
        }, 0);
        $('.js-compare-slider').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 6,
            slidesToScroll: 1,
            variableWidth: true,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><i class="icon-arrow-right"></i></button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><i class="icon-arrow-right"></i></button>',
            responsive: [{
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
        });
    };

    /* components begin*/
    if ($('#form-1').length) {
        $("#form-1").validate({
            rules: {
                name: "required",
                password: "required",
                email: "required"
            },
            messages: {
                name: "Это поле обязательное",
                password: "Это поле обязательное",
                email: {
                    required: "*Пожалуйста, введите ваш email"
                }
            }
        });
    }
    if ($('#form-2').length) {
        $("#form-2").validate({
            rules: {
                password: "required",
                email: "required"
            },
            messages: {
                password: "Это поле обязательное",
                email: {
                    required: "*Пожалуйста, введите ваш email"
                }
            }
        });
    }
    if ($('#form-3').length) {
        $("#form-3").validate({
            rules: {
                email: "required"
            },
            messages: {
                email: {
                    required: "*Пожалуйста, введите ваш email"
                }
            }
        });
    }    
    if ($('#form-4').length) {
        $("#form-4").validate({
            rules: {
                name: "required",
                email: "required",
                questions: "required"
            },
            messages: {
                name: "Это поле обязательное",
                email: {
                    required: "*Пожалуйста, введите ваш email"
                },
                questions: "Это поле обязательное",
            },
            submitHandler: function (form) {
              $('#form-4').parents('.subscribe').addClass('ok');
            }
        });
    } 
    if ($('.js-product-watched').length) {
        $('.js-product-watched').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><i class="icon-arrow-left"></i></button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><i class="icon-arrow-right"></i></button>',
            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 767,
                settings: {
                    variableWidth: true
                }
            }]
        });
    };



    if ($('.js-file').length) {
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(input).parent().css({
                        'background-image': 'url(' + e.target.result + ')'
                    });
                    $(input).parent().addClass('img-active');
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('.imgInp').each(function () {
            $(this).change(function () {
                readURL(this);
            });
        });
    };
    if ($('.js-scroll-horizontal').length) {
        $(".js-scroll-horizontal").mCustomScrollbar({
            axis: "x",
            theme: "dark-thin",
            scrollbarPosition: "outside",
            scrollButtons: {
                enable: true,
                scrollType: "stepped"
            },
            keyboard: {
                scrollType: "stepped"
            },
            mouseWheel: {
                normalizeDelta: true
            },
            autoExpandScrollbar: true,
            snapAmount: 203
        });
    };

    if ($('.js-styled-scroll').length) {
        $(".js-styled-scroll").mCustomScrollbar({
            axis: "y",
            theme: "dark-thin",
            scrollbarPosition: "outside",
        });
    };


    if ($('.js-rating').length) {
        $('.js-rating').addRating();
    };
    if ($('.js-fancybox').length) {
        $('.js-fancybox').fancybox({
            margin: 30,
            padding: 0,
            afterShow: function () {
                if ($('.js-popup-slider').length) {
                    $('.js-popup-slider').slick("getSlick").refresh();
                };
            }

        });
    };
    if ($('.js-popup-slider').length) {
        $('.js-popup-slider').slick({
            dots: true,
            infinite: false,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true
        });
    };
    if ($('.js-styled').length) {
        $('.js-styled').styler();
    };
    if ($('#average-highcharts').length) {
        Highcharts.setOptions({
            lang: {
                loading: 'Загрузка...',
                months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                shortMonths: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'],
                exportButtonTitle: "Экспорт",
                printButtonTitle: "Печать",
                rangeSelectorFrom: "С",
                rangeSelectorTo: "По",
                rangeSelectorZoom: "Период",
                downloadPNG: 'Скачать PNG',
                downloadJPEG: 'Скачать JPEG',
                downloadPDF: 'Скачать PDF',
                downloadSVG: 'Скачать SVG',
                printChart: 'Напечатать график'
            }
        });
        Highcharts.chart('average-highcharts', {
            chart: {
                type: 'spline',
                backgroundColor: 'transparent'
            },
            title: {
                text: ' '
            },
            subtitle: {
                text: ''
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            tooltip: {
                enabled: false
            },
            yAxis: [{
                gridLineWidth: 0,
                marker: {
                    states: {
                        hover: {
                            symbol: 'triangle',
                            radius: 5,
                        }
                    }
                },
                labels: {
                    enabled: false
                },
                title: {
                    enabled: false
                },
                min: 0,
                max: 250,
                tickAmount: 6,
                opposite: false
            }],
            legend: {
                enabled: false
            },
            xAxis: {
                type: 'datetime',
                gridLineColor: '#e8e8e8',
                gridLineWidth: 1,
                labels: {
                    style: {
                        color: '#8f939c',
                        fontSize: '12px',
                        fontWeight: '400',
                        fontFamily: 'Roboto'
                    },
                    format: '{value: %e %b}',
                },
            },
            colors: ['#af90d9'],
            series: [{
                name: 'RAM',
                data: [75.9, 70.5, 106.4],
                pointStart: Date.UTC(2018, 7, 7),
                pointInterval: 24 * 3600 * 500,
                marker: {
                    symbol: 'cross',
                    radius: 4,
                },
                title: {
                    text: 'X'
                },
            }]
        });
    };
    /*swipe begin*/
    $(".js-slider-big").swipe({
        swipeRight: function (event, direction, distance, duration, fingerCount, fingerData) {
            if ($('.js-slider-sm .googs-slider-sm__item.active').is(":first-child")) {
                $('.js-slider-sm .googs-slider-sm__item.active:first-child').parent().find('.googs-slider-sm__item:nth-child(4) a').click();
            } else {
                $('.js-slider-sm .googs-slider-sm__item.active').prev().find('a').click();
            }
        },
        swipeLeft: function (event, direction, distance, duration, fingerCount, fingerData) {
            if ($('.js-slider-sm .googs-slider-sm__item.active').is(":nth-child(4)")) {
                $('.js-slider-sm .googs-slider-sm__item.active:nth-child(4)').parent().find('.googs-slider-sm__item:first-child a').click();
            } else {
                $('.js-slider-sm .googs-slider-sm__item.active').next().find('a').click();
            }
        },
        excludedElements: $.fn.swipe.pageScroll.AUTO,
        threshold: 40,
        fingers: 'all',
        preventDefaultEvents: false
    });
    /*swipe end*/
    /* components end*/
    if ($('.js-top').length) {
        var scr_top = $(window).scrollTop();
        $(window).scroll(function () {
            if ($(window).scrollTop() >= 250) {
                $('.js-top').show(100);
            } else {
                $('.js-top').hide(100);
            }
        });
        $(window).on('load', function () {
            if ($(window).scrollTop() >= 250) {
                $('.js-top').show(100);
            } else {
                $('.js-top').hide(100);
            }
        });
    };




});

// var handler = function () {
//     var height_footer = $('footer').height();
//     var height_header = $('header').height();
//     //$('.content').css({'padding-bottom':height_footer+40, 'padding-top':height_header+40});
//
//     var viewport_wid = viewport().width;
//     var viewport_height = viewport().height;
//
//     if (viewport_wid <= 991) {
//
//     }
// }
// $(window).bind('load', handler);
// $(window).bind('resize', handler);



// A $( document ).ready() block.


/** catalog item info */

var handler = function () {


    //
    //
    //        $('.product-list__item').each(function (elm, item) {
    //
    //            var item = $('.product-list__item');
    //
    //            if ( $(this).hasClass('active') ) {
    //
    //                var prodItemOffset = $(this).offset().left;
    //                var prodMin = prodWrapOffset - prodItemOffset;
    //                var infoHeight = $(this).find('.product-list__info').outerHeight();
    //
    //                $(this).find('.product-list__info').css("left", prodMin);
    //                $(this).css('margin-bottom', infoHeight);
    //            }
    //
    //            else {
    //                $(this).css('margin-bottom', 0);
    //            }
    //        });



    $('.product-list__link').on('click', function () {
        var prodWrap = $('.js-product-catalog');
        var prodItem = $('.product-list__item').offset().left;

        //console.log(prodItem);
        var prodWrapOffset = prodWrap.offset().left;

        var item = $(this).parents('.product-list__item:not(.product-list__item_big-banner)');
        var itemHeight = $(this).parents('.product-list__item.active');
        var prodItemOffset = item.offset().left;
        var prodMin = prodWrapOffset - prodItemOffset;
        var infoHeight = item.find('.product-list__info').outerHeight();

        if (!item.hasClass('active')) {
            setTimeout(function () {
                item.addClass('active').siblings().removeClass('active').css('margin-bottom', 0);
                item.find('.product-list__info').css("left", prodMin);
                item.css('margin-bottom', infoHeight);
            }, 20)


        } else {
            setTimeout(function () {
                item.removeClass('active');
                item.find('.product-list__info').css("left", 0);
                item.css('margin-bottom', 0);
            }, 20)


        }



        // item.toggleClass('active').siblings().removeClass('active');
        // item.find('.product-list__info').css("left", prodMin);
        // itemHeight.css('margin-bottom', infoHeight);

        $(document).on('touchstart click', function (e) {
            var container = $(".product-list__info");
            if ($(e.target).parents('.product-list__item:not(.product-list__item_big-banner)').find('.product-list__info').length != 1) {
                container.parents('.product-list__item').removeClass('active');
                container.parents('.product-list__item').css('margin-bottom', '0');
            }
        });


        //return false;
    });


    var viewport_wid = viewport().width;
    if (viewport_wid > 767) {

        /** item */
        var max_col_height = 0;
        $('.product-list .product-list__item').each(function (index, item) {
            var thisItem = $(this);
            if (thisItem.height() > max_col_height) {
                max_col_height = thisItem.height();
            }
            $('.product-list .product-list__item').height(max_col_height);
            //console.log(max_col_height);
        });
        /** item */
    }

};
$(window).bind('load', handler);
$(window).bind('resize', handler);

/** catalog item info */
